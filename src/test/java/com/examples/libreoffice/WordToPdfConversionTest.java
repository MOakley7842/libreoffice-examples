package com.examples.libreoffice;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Created by moakley on 5/12/15.
 */
@RunWith(Parameterized.class)
public class WordToPdfConversionTest {
    @Parameterized.Parameters
    public static Collection sourceFiles() {
        return Arrays.asList(new Object[][]{
                {new WordToPdfConverter()}
        });
    }

    protected String sourceFile;
    protected String outputFile;
    protected WordToPdfConverter wordToPdfConverter;
    protected OutputStream outputStream;
    protected InputStream inputStream;

    public WordToPdfConversionTest(WordToPdfConverter converter)
    {
        String url = WordToPdfConversionTest.class.getResource("/test/image/").getPath();
        this.sourceFile = "/home/moakley/TestCases/images/Word/Template.doc";
        this.outputFile = "/home/moakley/TestCases/images/Word/Template.pdf";
//        this.sourceFile = url + "Template.doc";
//        this.outputFile = url + "Template.pdf";
//        this.sourceFile = url + "ICMArchitectureOverview.docx";
//        this.outputFile = url + "ICMArchitectureOverview.pdf";
        this.wordToPdfConverter = converter;
    }

    @Before
    public void setup() throws FileNotFoundException {
        outputStream = new ByteArrayOutputStream();
    }

    @After
    public void tearDown() throws IOException {
        if (outputStream != null) {
            File file = new File(outputFile);
            if (!file.exists())
                file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(((ByteArrayOutputStream) outputStream).toByteArray());
            fos.flush();
            fos.close();

            outputStream.close();
        }

        if (inputStream != null)
            inputStream.close();
    }

    @Test
    public void ShouldConvertSourceToDestination() throws IOException {
        inputStream = getSourceFile();
        runTest();
    }

    private void runTest() throws IOException {
        //wordToPdfConverter.convert(inputStream, outputStream);
        wordToPdfConverter.convert(sourceFile, outputFile);
    }

    protected InputStream getSourceFile() throws FileNotFoundException {
        return new FileInputStream(sourceFile);
    }
}
