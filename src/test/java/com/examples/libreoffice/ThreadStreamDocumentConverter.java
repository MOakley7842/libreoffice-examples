package com.examples.libreoffice;


import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.io.XInputStream;
import com.sun.star.io.XOutputStream;
import com.sun.star.lib.uno.adapter.ByteArrayToXInputStreamAdapter;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

import java.awt.*;
import java.io.*;
import java.util.List;

/**
 * Created by moakley on 6/3/15.
 */
public class ThreadStreamDocumentConverter {

    /** Containing the loaded documents
     */
    static com.sun.star.frame.XComponentLoader xCompLoader = null;
    /** Containing the given type to convert to
     */
    static String sConvertType = "writer_pdf_Export";
    /** Containing the given extension
     */
    static String sExtension = "pdf";
    /** Containing the current file or directory
     */
    static String sIndent = "";
    /** Containing the directory where the converted files are saved
     */
    static String sOutputDir = "/home/moakley/TestCases/Thread";
    static String sInputDir = "/home/moakley/TestCases/Thread";
    static Object lockObject = new Object();
    static int tCount = 0;

    static long lMillis = 0;


    private static void getComponentLoader() {

        com.sun.star.uno.XComponentContext xContext = null;

        try {
            // get the remote office component context
            xContext = com.sun.star.comp.helper.Bootstrap.bootstrap();
            //xContext = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
            System.out.println("Connected to a running office ...");

            // get the remote office service manager
            com.sun.star.lang.XMultiComponentFactory xMCF =
                    xContext.getServiceManager();

            Object oDesktop = xMCF.createInstanceWithContext(
                    "com.sun.star.frame.Desktop", xContext);

            xCompLoader = UnoRuntime.queryInterface(com.sun.star.frame.XComponentLoader.class,
                    oDesktop);
        } catch( Exception e ) {
            e.printStackTrace(System.err);
            System.exit(1);
        }

    }

    public ByteArrayOutputStream getByteArrayOutputStream(String filename) {
        ByteArrayOutputStream bytes = null;
        try {
            InputStream inputFile = new BufferedInputStream(new FileInputStream(filename));
            bytes = new ByteArrayOutputStream();
            byte[] byteBuffer = new byte[4096];
            int byteBufferLength = 0;
            while ((byteBufferLength = inputFile.read(byteBuffer)) > 0) {
                bytes.write(byteBuffer,0,byteBufferLength);
            }
            inputFile.close();
        }
        catch(Exception e) {}
        return bytes;
    }


    private static class ConvertLoop
            implements Runnable {
        public void run() {
            String outputDir = "";
            String inputDir = "";
            int count = 0;
            long localMillis = lMillis;
            long localMillisEnd = 0;
            long totalMillis = 0;
            ThreadStreamDocumentConverter tsdc = new ThreadStreamDocumentConverter();

            try {
                synchronized (lockObject) {
                    tCount++;
                    count = tCount;
                    outputDir = sOutputDir + count + "/pdf";
                    inputDir = sInputDir + count;
                }

                File dirInput = new File(inputDir);
                if ( !dirInput.isDirectory() ) {
                    throw new IllegalArgumentException(
                            "not a directory: " + dirInput.getName()
                    );
                }

                //File outdir = new File(outputDir);
                //String sOutUrl = outdir.getAbsolutePath().replace( '\\', '/' );

                System.out.println("Output directory: " + outputDir);

                System.out.println("Input directory:  " + inputDir);
                //System.out.println("Input directory:  " + dirInput.getName());

                // Getting all files and directories in the current directory
                File[] entries = dirInput.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        if (pathname.isFile())
                            return true;
                        else
                            return false;
                    }
                });

                for ( int i = 0; i < entries.length; ++i ) {
                    // Testing, if the entry in the list is a directory
                    // Converting the document to the favoured type
                    try {
                        ByteArrayOutputStream baos = tsdc.getByteArrayOutputStream(entries[i].getPath());
                        LOInputStream inputStream = new LOInputStream(baos.toByteArray());
                        // XInputStream inputStream = new  ByteArrayToXIntputStreamAdapter(baos.toByteArray());

                        localMillis = System.currentTimeMillis();

                        // Loading the wanted document
                        com.sun.star.beans.PropertyValue loadPropertyValues[] =
                                new com.sun.star.beans.PropertyValue[2];
                        loadPropertyValues[0] = new com.sun.star.beans.PropertyValue();
                        loadPropertyValues[0].Name = "InputStream";
                        loadPropertyValues[0].Value = inputStream;
                        loadPropertyValues[1] = new com.sun.star.beans.PropertyValue();
                        loadPropertyValues[1].Name = "Hidden";
                        loadPropertyValues[1].Value = Boolean.TRUE;

                        Object oDocToStore =
                                xCompLoader.loadComponentFromURL(
                                        "private:stream", "_blank", 0, loadPropertyValues);

                        com.sun.star.frame.XStorable xStorable =
                                UnoRuntime.queryInterface(
                                        com.sun.star.frame.XStorable.class, oDocToStore );

                        //XOutputStream outputStream = new ByteArrayToXIntputStreamAdapter(new byte[0]);
                        LOOutputStream outputStream = new LOOutputStream();
                        // Preparing properties for converting the document
                        com.sun.star.beans.PropertyValue storePropertyValues[] = new com.sun.star.beans.PropertyValue[2];
                        // Setting the output stream
                        storePropertyValues[0] = new com.sun.star.beans.PropertyValue();
                        storePropertyValues[0].Name = "OutputStream";
                        storePropertyValues[0].Value = outputStream;
                        // Setting the filter name
                        storePropertyValues[1] = new com.sun.star.beans.PropertyValue();
                        storePropertyValues[1].Name = "FilterName";
                        storePropertyValues[1].Value = sConvertType;


                        // Storing and converting the document
                        xStorable.storeToURL("private:stream", storePropertyValues);

                        // Closing the converted document. Use XCloseable.close if the
                        // interface is supported, otherwise use XComponent.dispose
                        com.sun.star.util.XCloseable xCloseable =
                                UnoRuntime.queryInterface(
                                        com.sun.star.util.XCloseable.class, xStorable);

                        if ( xCloseable != null ) {
                            xCloseable.close(false);
                        } else {
                            com.sun.star.lang.XComponent xComp =
                                    UnoRuntime.queryInterface(
                                            com.sun.star.lang.XComponent.class, xStorable);

                            xComp.dispose();
                        }

                        localMillisEnd = System.currentTimeMillis();

                        tsdc.writeOutputStream(entries[i].getPath(), outputDir, outputStream);
                    }
                    catch( Exception e ) {
                        e.printStackTrace(System.err);
                    }

                    //long lLocal = System.currentTimeMillis();
                    System.out.println(count + "\t" + entries[ i ].getName() +"\t Milliseconds:  " + (localMillisEnd - localMillis));
                    totalMillis += (localMillisEnd - localMillis);
                    //localMillis = lLocal;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("Total Millis:  " + totalMillis);
        }
    }


    //Write out the converted document
    public void writeOutputStream(String filePath, String outputDir, LOOutputStream outputStream) {
        try {
            // Appending the favoured extension to the origin document name
            int index1 = filePath.lastIndexOf('/');
            int index2 = filePath.lastIndexOf('.');
            String sStoreUrl = outputDir + filePath.substring(index1, index2 + 1)
                    + sExtension;

            FileOutputStream outputFile = new FileOutputStream(sStoreUrl);
            outputFile.write(outputStream.toByteArray());
            outputFile.close();
        }
        catch(IOException ioe) {}
    }

    // Display a message, preceded by
    // the name of the current thread
    static void threadMessage(String message) {
        String threadName =
                Thread.currentThread().getName();
        System.out.format("%s: %s%n",
                threadName,
                message);
    }



    public static void main( String args[] ) {
        com.sun.star.uno.XComponentContext xContext = null;

        try {
            lMillis = System.currentTimeMillis();
            System.out.println("Milliseconds:  " + lMillis);

            getComponentLoader();

            int count = 1;

            threadMessage("Starting MessageLoop thread");
            Thread [] threads = new Thread[count];

            for (int i = 0; i < count; i++) {
                threads[i] = new Thread(new ConvertLoop());
                threads[i].start();
            }

            for(int i = 0; i < threads.length; i++)
                threads[i].join();

            threadMessage("Finished:  " + (System.currentTimeMillis() - lMillis));


            System.exit(0);
        } catch( Exception e ) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
