package com.examples.libreoffice;

import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.List;

/**
 * Created by Mike.Oakley on 5/29/2015.
 */
public class ThreadDocumentConverter {

    /** Containing the loaded documents
     */
    static com.sun.star.frame.XComponentLoader xCompLoader = null;
    /** Containing the given type to convert to
     */
    static String sConvertType = "writer_pdf_Export";
    /** Containing the given extension
     */
    static String sExtension = "pdf";
    /** Containing the current file or directory
     */
    static String sIndent = "";
    /** Containing the directory where the converted files are saved
     */
    static String sOutputDir = "/home/moakley/TestCases/Thread";
    static String sInputDir = "/home/moakley/TestCases/Thread";
    static Object lockObject = new Object();
    static int tCount = 0;

    static long lMillis = 0;

    private static void getComponentLoader() {

        com.sun.star.uno.XComponentContext xContext = null;

        try {
            // get the remote office component context
            xContext = com.sun.star.comp.helper.Bootstrap.bootstrap();
            //xContext = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
            System.out.println("Connected to a running office ...");

            // get the remote office service manager
            com.sun.star.lang.XMultiComponentFactory xMCF =
                    xContext.getServiceManager();

            Object oDesktop = xMCF.createInstanceWithContext(
                    "com.sun.star.frame.Desktop", xContext);

            xCompLoader = UnoRuntime.queryInterface(com.sun.star.frame.XComponentLoader.class,
                    oDesktop);
        } catch( Exception e ) {
            e.printStackTrace(System.err);
            System.exit(1);
        }

    }

    private static class ConvertLoop
            implements Runnable {
        public void run() {
            String outputDir = "";
            String inputDir = "";
            int count = 0;
            long localMillis = lMillis;
            long totalMillis = 0;

            try {
                synchronized (lockObject) {
                    tCount++;
                    count = tCount;
                    outputDir = sOutputDir + count + "/pdf";
                    inputDir = sInputDir + count;
                }

                File dirInput = new File(inputDir);
                if (!dirInput.isDirectory()) {
                    throw new IllegalArgumentException(
                            "not a directory: " + dirInput.getName()
                    );
                }

                File outdir = new File(outputDir);
                String sOutUrl = "file:///" + outdir.getAbsolutePath().replace('\\', '/');

                System.out.println("Output directory: " + outdir.getPath());

                System.out.println("Input directory:  " + dirInput.getPath());
                //System.out.println("Input directory:  " + dirInput.getName());

                // Getting all files and directories in the current directory
                File[] entries = dirInput.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        if (pathname.isFile())
                            return true;
                        else
                            return false;
                    }
                });

                for (int i = 0; i < entries.length; ++i) {
                    // Testing, if the entry in the list is a directory
                    // Converting the document to the favoured type
                    try {
                        // Composing the URL by replacing all backslashs
                        String sUrl = "file:///"
                                + entries[i].getAbsolutePath().replace('\\', '/');

                        // Loading the wanted document
                        com.sun.star.beans.PropertyValue loadPropertyValues[] =
                                new com.sun.star.beans.PropertyValue[1];
                        loadPropertyValues[0] = new com.sun.star.beans.PropertyValue();
                        loadPropertyValues[0].Name = "Hidden";
                        loadPropertyValues[0].Value = Boolean.TRUE;

                        //System.out.println(count + "\t" + entries[ i ].getName() +"\t Load source file ");
                        Object oDocToStore =
                                xCompLoader.loadComponentFromURL(
                                        sUrl, "_blank", 0, loadPropertyValues);

                        // get page count  ThisComponent.CurrentController.PageCount

                        //System.out.println(count + "\t" + entries[ i ].getName() +"\t Create xStorable ");
                        // Getting an object that will offer a simple way to store
                        // a document to a URL.
                        com.sun.star.frame.XStorable xStorable =
                                UnoRuntime.queryInterface(
                                        com.sun.star.frame.XStorable.class, oDocToStore);

                        // Preparing properties for converting the document
                        com.sun.star.beans.PropertyValue storePropertyValues[] = new com.sun.star.beans.PropertyValue[2];
                        // Setting the flag for overwriting
                        storePropertyValues[0] = new com.sun.star.beans.PropertyValue();
                        storePropertyValues[0].Name = "FilterName";
                        storePropertyValues[0].Value = sConvertType;
                        // Setting the filter name
                        storePropertyValues[1] = new com.sun.star.beans.PropertyValue();
                        storePropertyValues[1].Name = "Overwrite";
                        storePropertyValues[1].Value = Boolean.TRUE;

                        // Appending the favoured extension to the origin document name
                        int index1 = sUrl.lastIndexOf('/');
                        int index2 = sUrl.lastIndexOf('.');
                        String sStoreUrl = sOutUrl + sUrl.substring(index1, index2 + 1)
                                + sExtension;

                        //System.out.println(count + "\t" + entries[ i ].getName() +"\t Store as PDF ");
                        // Storing and converting the document
                        xStorable.storeToURL(sStoreUrl, storePropertyValues);

                        // Closing the converted document. Use XCloseable.close if the
                        // interface is supported, otherwise use XComponent.dispose
                        com.sun.star.util.XCloseable xCloseable =
                                UnoRuntime.queryInterface(
                                        com.sun.star.util.XCloseable.class, xStorable);

                        if (xCloseable != null) {
                            xCloseable.close(false);
                        } else {
                            com.sun.star.lang.XComponent xComp =
                                    UnoRuntime.queryInterface(
                                            com.sun.star.lang.XComponent.class, xStorable);

                            xComp.dispose();
                        }
                    } catch (Exception e) {
                        e.printStackTrace(System.err);
                    }

                    long lLocal = System.currentTimeMillis();
                    System.out.println(count + "\t" + entries[i].getName() + "\t Milliseconds:  " + (lLocal - localMillis));
                    totalMillis += (lLocal - localMillis);
                    localMillis = lLocal;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("Total Millis:  " + totalMillis);
        }
    }


    // Display a message, preceded by
    // the name of the current thread
    static void threadMessage(String message) {
        String threadName =
                Thread.currentThread().getName();
        System.out.format("%s: %s%n",
                threadName,
                message);
    }

    public static void main( String args[] ) {
        com.sun.star.uno.XComponentContext xContext = null;

        try {
            lMillis = System.currentTimeMillis();
            System.out.println("Milliseconds:  " + lMillis);

            getComponentLoader();

            int count = 4;

            threadMessage("Starting MessageLoop thread");
            Thread [] threads = new Thread[count];

            for (int i = 0; i < count; i++) {
                threads[i] = new Thread(new ConvertLoop());
                threads[i].start();
            }

            for(int i = 0; i < threads.length; i++)
                threads[i].join();

            threadMessage("Finished");


            System.exit(0);
        } catch( Exception e ) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
