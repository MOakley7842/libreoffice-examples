package com.examples.libreoffice;

import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.document.XDocumentProperties;
import com.sun.star.document.XDocumentPropertiesSupplier;
import com.sun.star.uno.UnoRuntime;

import com.sun.star.uno.XComponentContext;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * Created by moakley on 6/17/15.
 */
public class LOWordToPdfConverter {

    /**
     * Containing the loaded documents
     */
    static com.sun.star.frame.XComponentLoader xCompLoader = null;
    /**
     * Containing the given type to convert to
     */
    static String sConvertType = "writer_pdf_Export";


    private static void getComponentLoader() {

        com.sun.star.uno.XComponentContext xContext = null;

        try {
            // get the remote office component context
            xContext = com.sun.star.comp.helper.Bootstrap.bootstrap();
            //xContext = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
            System.out.println("Connected to a running office ...");

            // get the remote office service manager
            com.sun.star.lang.XMultiComponentFactory xMCF =
                    xContext.getServiceManager();

            Object oDesktop = xMCF.createInstanceWithContext(
                    "com.sun.star.frame.Desktop", xContext);

            xCompLoader = UnoRuntime.queryInterface(com.sun.star.frame.XComponentLoader.class,
                    oDesktop);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            //System.exit(1);
        }

    }

    public void convert(InputStream inputStream, OutputStream outputStream) throws IOException {
        try {
            if (xCompLoader == null)
                getComponentLoader();

            LOInputStream loInputStream = new LOInputStream(IOUtils.toByteArray(inputStream));

            // Loading the wanted document
            com.sun.star.beans.PropertyValue loadPropertyValues[] =
                    new com.sun.star.beans.PropertyValue[2];
            loadPropertyValues[0] = new com.sun.star.beans.PropertyValue();
            loadPropertyValues[0].Name = "InputStream";
            loadPropertyValues[0].Value = loInputStream;
            loadPropertyValues[1] = new com.sun.star.beans.PropertyValue();
            loadPropertyValues[1].Name = "Hidden";
            loadPropertyValues[1].Value = Boolean.TRUE;

            Object oDocToStore =
                    xCompLoader.loadComponentFromURL(
                            "private:stream", "_blank", 0, loadPropertyValues);

            int pageCount = getPageCount(oDocToStore);

            com.sun.star.frame.XStorable xStorable =
                    UnoRuntime.queryInterface(
                            com.sun.star.frame.XStorable.class, oDocToStore);

            LOOutputStream loOutputStream = new LOOutputStream();

            // Preparing properties for converting the document
            com.sun.star.beans.PropertyValue storePropertyValues[] = new com.sun.star.beans.PropertyValue[3];
            // Setting the output stream
            storePropertyValues[0] = new com.sun.star.beans.PropertyValue();
            storePropertyValues[0].Name = "OutputStream";
            storePropertyValues[0].Value = loOutputStream;
            // Setting the filter name
            storePropertyValues[1] = new com.sun.star.beans.PropertyValue();
            storePropertyValues[1].Name = "FilterName";
            storePropertyValues[1].Value = sConvertType;
            // Setting the flag for overwriting
            com.sun.star.beans.PropertyValue filterPropertyValues[] = new com.sun.star.beans.PropertyValue[1];
            //filterPropertyValues[0] = new com.sun.star.beans.PropertyValue();
            //filterPropertyValues[0].Name = "OutputStream";
            //filterPropertyValues[0].Value = loOutputStream;
            // Setting the Page range
            filterPropertyValues[0] = new com.sun.star.beans.PropertyValue();
            filterPropertyValues[0].Name = "PageRange";
            if (pageCount > 5)
                filterPropertyValues[0].Value = "3;4";
            else
                filterPropertyValues[0].Value = "1;2";

            storePropertyValues[2] = new com.sun.star.beans.PropertyValue();
            storePropertyValues[2].Name = "FilterData";
            storePropertyValues[2].Value = filterPropertyValues;


            // Storing and converting the document
            xStorable.storeToURL("private:stream", storePropertyValues);

            // Closing the converted document. Use XCloseable.close if the
            // interface is supported, otherwise use XComponent.dispose
            com.sun.star.util.XCloseable xCloseable =
                    UnoRuntime.queryInterface(
                            com.sun.star.util.XCloseable.class, xStorable);

            if (xCloseable != null) {
                xCloseable.close(false);
            } else {
                com.sun.star.lang.XComponent xComp =
                        UnoRuntime.queryInterface(
                                com.sun.star.lang.XComponent.class, xStorable);

                xComp.dispose();
            }

            loOutputStream.writeTo(outputStream);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

    }


    static int getPageCount(Object oDoc) {
        int pageCount = 0;

        XDocumentPropertiesSupplier xDPS = (XDocumentPropertiesSupplier)
                UnoRuntime.queryInterface(XDocumentPropertiesSupplier.class, oDoc);
        XDocumentProperties docProperties = xDPS.getDocumentProperties();

        com.sun.star.beans.NamedValue[] docNameValues = docProperties.getDocumentStatistics();
        for (int j = 0; j < docNameValues.length; j++) {
            if (docNameValues[j].Name.equals("PageCount"))
                pageCount = Integer.parseInt(docNameValues[j].Value.toString());

            System.out.println(docNameValues[j].Name + " : " + docNameValues[j].Value.toString());
        }

        return pageCount;
    }
}
