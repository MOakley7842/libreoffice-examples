package com.examples.libreoffice;

import com.sun.star.beans.PropertyValue;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.connection.NoConnectException;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.comp.helper.Bootstrap;

import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XStorable;

import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created by moakley on 5/12/15.
 */
public class WordToPdfConverter {

    public static void initialize() {
        try {
            System.out.println(System.getProperty("java.library.path"));

            // get the remote office component context
            XComponentContext xContext = Bootstrap.bootstrap();

            System.out.println("Connected to a running office ...");

            XMultiComponentFactory xMCF = xContext.getServiceManager();

            String available = (xMCF != null ? "available" : "not available");
            System.out.println( "remote ServiceManager is " + available );
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    //public void convert(InputStream inputStream, OutputStream outputStream) throws IOException {
    public void convert(String filename, String filesave) throws IOException {

        try {
            XComponentContext xLocalContext = Bootstrap.createInitialComponentContext(null);
            System.out.println("xLocalContext");

            XMultiComponentFactory xLocalServiceManager = xLocalContext.getServiceManager();
            System.out.println("xLocalServiceManager");

            Object urlResolver = xLocalServiceManager.createInstanceWithContext(
                    "com.sun.star.bridge.UnoUrlResolver", xLocalContext);
            System.out.println("urlResolver");

            XUnoUrlResolver xUrlResolver =
                    (XUnoUrlResolver) UnoRuntime.queryInterface(XUnoUrlResolver.class, urlResolver);
            System.out.println("xUrlResolve");

            try {
                String uno = "uno:socket,host=127.0.0.1,port=8100;urp;StarOffice.ServiceManager";
                Object rInitialObject = xUrlResolver.resolve(uno);
                System.out.println("rInitialObject");

                if (null != rInitialObject) {
                    XMultiComponentFactory xOfficeFactory = (XMultiComponentFactory) UnoRuntime.queryInterface(
                            XMultiComponentFactory.class, rInitialObject);
                    System.out.println("xOfficeFactory");

                    Object desktop = xOfficeFactory.createInstanceWithContext("com.sun.star.frame.Desktop", xLocalContext);
                    System.out.println("desktop");

                    XComponentLoader xComponentLoader = (XComponentLoader)UnoRuntime.queryInterface(
                            XComponentLoader.class, desktop);
                    System.out.println("xComponentLoader");

                    PropertyValue[] loadProps = new PropertyValue[1];

                    loadProps[0] = new PropertyValue();
                    loadProps[0].Name = "Hidden";
                    loadProps[0].Value = Boolean.TRUE;

                    try {
                        String filenameUrl = new File(filename).toURI().toURL().toString();
                        XComponent xComponent = xComponentLoader.loadComponentFromURL(filenameUrl, "_blank", 0, loadProps);
                        System.out.println("xComponent from " + filename);

                        XStorable xStore = (XStorable)UnoRuntime.queryInterface(XStorable.class, xComponent);
                        System.out.println("xStore");

                        loadProps = new PropertyValue[2];

                        System.out.println("Overwrite");
                        loadProps[0].Name = "Overwrite";
                        loadProps[0].Value = Boolean.TRUE;

                        System.out.println("writer_pdf_Export");
                        loadProps[1].Name = "FilterName";
                        loadProps[1].Value = "writer_pdf_Export";

                        String filesaveUrl = new File(filesave).toURI().toURL().toString();
                        xStore.storeToURL(filesaveUrl, loadProps);
                        System.out.println("storeToURL to file " + filesave);

                        xComponent.dispose();

                        xComponentLoader = null;
                        rInitialObject = null;

                        System.out.println("done.");

                    } catch (IllegalArgumentException e) {
                        System.err.println("Error: Can't load component from url " + filename);
                        e.printStackTrace();
                    }
                } else {
                    System.err.println("Error: Unknown initial object name at server side");

                }
            } catch(NoConnectException e) {
                System.err.println("Error: Server Connection refused: check server is listening...");
                e.printStackTrace();
            }
        } catch(java.lang.Exception e) {
            System.err.println("Error: Java exception:");
            e.printStackTrace();
        }
    }
/*
    private String convertToURL(String filename) throws IOException {
        StringBuffer sbURL = new StringBuffer("file:///");
        java.io.File sourceFile = new java.io.File(filename);
        sbURL.append(sourceFile.getCanonicalPath().replace('\\', '/'));
        return sbURL.toString();
    }
*/
    /*
    public static String convertToURL(String pFilePath, XComponentContext pComponentContext ) throws MalformedURLException {

        String internalUrl = null;

        URL externalUrl = new File(pFilePath).toURI().toURL();
        XExternalUriReferenceTranslator translator = ExternalUriReferenceTranslator.create(
                pComponentContext);
        internalUrl =  translator.translateToInternal(externalUrl.toExternalForm());

        return internalUrl;
    }
*/
}
